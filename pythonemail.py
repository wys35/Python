# https://stackoverflow.com/questions/3362600/how-to-send-email-attachments/3363254#3363254
# https://pybit.es/python-MIME-bcc.html
import smtplib
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate

def send_mail(send_from, send_to, subject, text, files=None, server="127.0.0.1"):
	assert isinstance(send_to, list)
	msg = MIMEMultipart()
	msg['From'] = send_from
	msg['To'] = COMMASPACE.join(send_to)
	msg['Date'] = formatdate(localtime=True)
	msg['Subject'] = subject
	msg.attach(MIMEText(text, 'plain'))

	files = ["C:\\Users\\ewu\\Desktop\\GitHub\\Python\\MagazineLib\\test.zip"]

	for f in files or []:
		with open(f, "rb") as fil:
			part = MIMEApplication(fil.read(), Name=basename(f))
		part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
		msg.attach(part)

	smtpObj = smtplib.SMTP(server)
	#smtpObj.login('','')
	smtpObj.sendmail(send_from, send_to, msg.as_string())
	smtpObj.close()

import time, datetime
def calcProd():
    product = 1
    for i in range(1,100000):
        product = product * i
    return product

startTime = time.time()
prod = calcProd()
endTime = time.time()
print('The result is %s digits long.' % (len(str(prod))))
print('Took %s seconds to calculate.' % (endTime - startTime))

print(datetime.datetime.now())
delta = datetime.timedelta(days=11, hours=10, minutes=9, seconds=8)
delta.total_seconds()
print(str(delta))
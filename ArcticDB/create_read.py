from arcticdb import Arctic

ac = Arctic('lmdb://c:/temp/ac.db')
#ac.create_library('data')
ac.list_libraries()
library = ac['data']

from datetime import datetime
import pandas as pd
import numpy as np
cols = ['COL_%d' % i for i in range(50)]
df = pd.DataFrame(np.random.randint(0, 50, size=(25, 50)), columns=cols)
df.index = pd.date_range(datetime(2000, 1, 1, 5), periods=25, freq="H")
#library.write('test_frame', df)

data = library.read("test_frame")
print(data.data)
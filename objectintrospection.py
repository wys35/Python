my_list = [1,2,3]
dir(my_list)

print(type(''))
print(type([]))
print(type({}))
print(type(dict))
print(type(3))

name = 'Yahoo'
print(id(name))

import inspect
print(inspect.getmembers(str))

print(name.__add__('test'))
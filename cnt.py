from collections import Counter
colors = (
    ('Yasoob', 'Yellow'),
    ('Ali', 'Blue'),
    ('Arham', 'Green'),
    ('Ali', 'Black'),
    ('Yasoob', 'Red'),
    ('Ahmed', 'Silver')
)

favs = Counter(name for name, color in colors)
print(favs)

#
with open('filename', 'rb') as f:
    line_count = Counter(f)
print(line_count)
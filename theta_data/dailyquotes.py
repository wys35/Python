import pandas as pd
from IPython.display import display
from datetime import date
from thetadata import ThetaClient, OptionReqType, OptionRight, DateRange

client = ThetaClient()  # We don't need to provide a username / password because this is a free request.

with client.connect():  # Make any requests for data inside this block. Requests made outside this block wont run.
    data = client.get_hist_option(
          req=OptionReqType.EOD,
          root="AAPL",
          exp=date(2022, 11, 25),
          strike=150,
          right=OptionRight.CALL,
          date_range=DateRange(date(2022, 10, 15), date(2022, 11, 15))
    )
    
# We are out of the client.connect() block, so we can no longer make requests.   
display(data)  # displays the data. You can also do print(data).